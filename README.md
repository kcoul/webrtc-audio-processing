# About (from upstream README.md)
This is meant to be a more Linux packaging friendly copy of the AudioProcessing
module from the [ WebRTC ](https://webrtc.googlesource.com/src) project. The
ideal case is that we make no changes to the code to make tracking upstream
code easy.

This package currently only includes the AudioProcessing bits, but I am very
open to collaborating with other projects that wish to distribute other bits of
the code and hopefully eventually have a single point of packaging all the
WebRTC code to help people reuse the code and avoid keeping private copies in
several different projects.

# Building

This project uses the [Meson build system](https://mesonbuild.com/). The
quickest way to build is described below. 

Both static and shared library builds will be produced. 
This can be changed by setting `default_library` in the top-level meson.build file.

### Linux / macOS

```sh
# Initialise into the build/ directory, for a prefixed install into the
# install/ directory 

# Debug Build

mkdir install

meson . build -Dprefix=$PWD/install

# Release Build

meson . build -Dprefix=$PWD/install --buildtype=release

# Run the actual build
ninja -C build

# Install locally
ninja -C build install

# The libraries, headers, and pkg-config files are now in the install/
# directory
```
To get the macOS builds for both **x86** and **arm** you can run the above commands
from the Rosetta terminal and ARM native terminal respectively. Alternatively use cross-files (see Cross-platform building (desktop)).

### Windows
```sh
# On Windows, use VS Command Prompt and specify absolute path in place of $PWD
# Initialise the build as above, but use x64 Native Tools Command Prompt for 64-bit builds, 
# and regular Developer Command Prompt for 32-bit builds

# With x64 Native Tools Command Prompt use Meson Wrapper.
"C:\Program Files\Meson\meson.exe" compile -C build
```

# Cross-platform building (mobile)
### iOS
```sh
# Build files will be written into build-ios folder.

meson build-ios/ --cross-file crossfile-ios.txt

ninja -C build-ios
```

### iOS simulator (x86_64)
```sh
# Build files will be written into build-ios-sim-x86 folder.

meson build-ios-sim-x86/ --cross-file crossfile-ios-sim-x86-64.txt

ninja -C build-ios-sim-x86
```

### iOS simulator (arm64)
```sh
# Build files will be written into build-ios-sim-arm folder.

meson build-ios-sim-arm/ --cross-file crossfile-ios-sim-arm64.txt

ninja -C build-ios-sim-arm
```

### Android
```sh
# TO DO
```

# Cross-platform building (desktop)

### macOS (x86_64)
```sh
# Build files will be written into build-macos-x86 folder.

meson build-macos-x86/ --cross-file crossfile-macos-x86-64.txt

ninja -C build-macos-x86
```

### macOS (arm64)
```sh
# Build files will be written into build-macos-arm folder.

meson build-macos-arm/ --cross-file crossfile-macos-arm64.txt

ninja -C build-macos-arm
```

# Feedback (from upstream README.md)

Patches, suggestions welcome. You can file an issue on our Gitlab
[repository](https://gitlab.freedesktop.org/pulseaudio/webrtc-audio-processing/).

# Notes (from upstream README.md)

1. It might be nice to try LTO on the library. We build a lot of code as part
   of the main AudioProcessing module deps, and it's possible that this could
   provide significant space savings.
