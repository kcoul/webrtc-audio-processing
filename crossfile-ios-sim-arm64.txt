[host_machine]
system = 'darwin'
cpu_family = 'aarch64'
cpu = 'arm64'
endian = 'little'

[properties]
c_args = ['-arch', 'arm64', '-miphonesimulator-version-min=11.0', '-isysroot', '/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk']
cpp_args = ['-stdlib=libc++', '-arch', 'arm64', '-miphonesimulator-version-min=11.0', '-isysroot', '/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk']
c_link_args = ['-arch', 'arm64', '-miphonesimulator-version-min=11.0', '-isysroot', '/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk']
cpp_link_args = ['-arch', 'arm64', '-miphonesimulator-version-min=11.0', '-isysroot', '/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk']

[binaries]
c = ['clang']
cpp = ['clang++']
ar = ['ar']
strip = ['strip']
